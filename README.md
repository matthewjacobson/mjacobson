# mjacobson.dev

A small site about me/ something I use to try out test tech.

## Site
* [nuxtjs](https://nuxtjs.org)
* [tailwindcss](https://tailwindcss.com)
* [tailblocks](https://tailblocks.cc)
* [rellax](https://dixonandmoe.com/rellax)
* [vue-rellax](https://github.com/croutonn/vue-rellax)
* [vue-scrollto](https://www.npmjs.com/package/vue-scrollto)
* [vue-js-modal](https://www.npmjs.com/package/vue-js-modal)
* [fontawesome](https://fontawesome.com)

## Infra
* [aws](https://aws.amazon.com) - S3
* [cloudflare](https://www.cloudflare.com) - DNS, SSL, Cache, Workers
* [google domains](https://domains.google) - domain
* [gitlab](https://gitlab.com) - repo, pipelines
* [docker](https://www.docker.com) - enables testing

## Test
* [cypress](https://www.cypress.io)
    * UI automation
        * runs against localhost on branch
        * runs against live site after deploy
* [lighthouse](https://developers.google.com/web/tools/lighthouse)
    * monitor performance
        * runs on live site after deploy
* [snyk](https://snyk.io)
    * monitor security
        * runs on MR creation
* [gitlab SAST](https://docs.gitlab.com/ee/user/application_security/sast/)
    * monitor security
        * runs when code is pushed to branch 

## Acknowledgments
* See above 👆 for all the open source and free tier solutions used in this project
