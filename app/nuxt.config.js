export default {
  ssr: false,
  target: 'static',

  head: {
    title: 'mjacobson',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'An about me page for Matt Jacobson, software engineering, test engineering, test automation, and more' }
    ],
    link: [
      {
        rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Rubik+Mono+One&display=swap'
      }
    ]
  },

  plugins: [
    { src: '~~/node_modules/vue-rellax/lib/nuxt-plugin', ssr: false },
    '~plugins/vue-js-modal.js'
  ],

  components: true,

  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/tailwindcss'
  ],

  helmet: {
  },

  pwa: {
    meta: {
      name: 'mjacobson.dev',
      author: 'mjacobson',
      description: 'An about me page for Matt Jacobson, software engineering, test engineering, test automation, and more',
      theme_color: '#646464'
    },
    manifest: {
      lang: 'en',
      name: 'mjacobson.dev',
      short_name: 'mjacobson.dev',
      description: 'An about me page for Matt Jacobson, software engineering, test engineering, test automation, and more',
      start_url: 'https://www.mjacobson.dev/',
      background_color: '#646464'
    }
  },

  modules: [
    'nuxt-helmet',
    '@nuxtjs/pwa',
    'nuxt-svg-loader',
    '@nuxtjs/axios',
    'vue-scrollto/nuxt',
    [
      '@nuxtjs/pwa',
      {
        workbox: {
          clientsClaim: false
        }
      }
    ]
  ],

  generate: {
    dir: 'public'
  }
}
