module.exports = {
  content: [
    './pages/index.vue',
    './layouts/default.vue',
    './components/**/*.vue'
  ],
  theme: {
    extend: {}
  },
  variants: {
    extend: {}
  },
  plugins: []
}
