module.exports = {
  ci: {
    collect: {
      url: ['https://www.mjacobson.dev'],
      settings: { chromeFlags: '--no-sandbox' }
    },
    upload: {
      target: 'temporary-public-storage'
    },
    assert: {
      assertions: {
        'categories:performance': ['error', { minScore: 0.8 }],
        'categories:best-practices': ['error', { minScore: 1 }],
        'categories:seo': ['error', { minScore: 1 }],
        'categories:pwa': ['error', { minScore: 0.9 }],
        'uses-optimized-images': ['error', { minScore: 1 }],
        'uses-responsive-images': ['error', { maxLength: 0 }]
      }
    }
  }
}
