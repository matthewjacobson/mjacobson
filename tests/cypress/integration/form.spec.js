/* eslint-disable */
/// <reference types="cypress" />

context('Window', () => {
  beforeEach(() => {
    cy.visit('/')
    cy.contains('Contact').click()
  })

  it('should perform form validation on the name field', () => {
    cy.get('#email').type('email@email.com')
    cy.get('#message').type('Im a message')
    cy.contains('Submit').click()

    cy.get('#name')
      .should('have.attr', 'placeholder')
      .should('include', 'Name is Required')

    cy.get('#name')
      .should('have.class', 'bg-red-100')
      .should('have.class', 'border-red-300')
  })

  it('should perform form validation on the email field', () => {
    cy.get('#name').type('Im a name')
    cy.get('#message').type('Im a message')
    cy.contains('Submit').click()

    cy.get('#email')
      .should('have.attr', 'placeholder')
      .should('include', 'Email is Required')

    cy.get('#email')
      .should('have.class', 'bg-red-100')
      .should('have.class', 'border-red-300')
  })

  it('should perform form validation on the message field', () => {
    cy.get('#name').type('Im a name')
    cy.get('#email').type('email@email.com')
    cy.contains('Submit').click()

    cy.get('#message')
      .should('have.attr', 'placeholder')
      .should('include', 'Message is Required')

    cy.get('#message')
      .should('have.class', 'bg-red-100')
      .should('have.class', 'border-red-300')
  })

  it('should display message after contact form is sent', () => {
    cy.get('#name').type('Im a name')
    cy.get('#email').type('email@email.com')
    cy.get('#message').type('Im a message')

    // Comment out so I don’t abuse formspree
    // cy.contains('Submit').click()
    // cy.contains('Thank you!')
  })

  it('should contain a link to linkedin', () => {
    cy.get('#linkedin').should('have.attr', 'href').and('include', 'https://www.linkedin.com/in/jacobsonm1')
  })

  it('should contain a link to gitlab', () => {
    cy.get('#gitlab').should('have.attr', 'href').and('include', 'https://gitlab.com/matthewjacobson')
  })

  it('should contain a link to frustratedrobot', () => {
    cy.get('#frustratedrobot').should('have.attr', 'href').and('include', 'https://www.frustratedrobot.com')
  })
})
