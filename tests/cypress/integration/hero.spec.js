/* eslint-disable */
/// <reference types="cypress" />

context('Window', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('should contain mjacobson as the title', () => {
    cy.title().should('include', 'mjacobson')
  })

  it('should contain a contact button that leads to a form', () => {
    cy.contains('Contact').click()
    cy.get('#name').should('have.attr', 'placeholder').and('include', 'Name')
    cy.get('#email').should('have.attr', 'placeholder').and('include', 'Email')
    cy.get('#message').should('have.attr', 'placeholder').and('include', 'Message')
  })

  it('should contain a link to the source code', () => {
    cy.get('#repo').should('have.attr', 'href').and('include', 'https://gitlab.com/matthewjacobson/mjacobson')
  })
})
